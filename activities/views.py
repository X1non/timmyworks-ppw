from django.shortcuts import render, redirect
from .models import *
from .forms import *
# Create your views here.

def list_activities(request):
    list_activities = Activity.objects.all()
    return render(request, 'activities/list_activities.html', {'list_activities': list_activities})

def create_activity(request):
    form = ActivityForm()

    if request.method == "POST":
        form = ActivityForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/activities')

    context = {'form_activ':form}
    return render(request, 'activities/activities_form.html', context)

def edit_activity(request, name):
    activity = Activity.objects.get(name=name)
    form = ActivityForm(instance=activity)

    if request.method == "POST":
        form = ActivityForm(request.POST, instance=activity)
        if form.is_valid():
            form.save()
            return redirect('/activities')

    context = {'form_activ':form}
    return render(request, 'activities/activities_form.html', context)


def delete_activity(request, name):
    activity = Activity.objects.get(name=name)

    if request.method == "POST":
        activity.delete()
        return redirect("/activities")

    context = {'activity':activity}
    return render(request, 'activities/list_activities.html', context)

def add_participants(request, activity_id):
    activity = Activity.objects.get(id=activity_id)
    form = ParticipantForm()

    if request.method == "POST":
        form = ParticipantForm(request.POST)    # hasil inputan (request.POST) ParticipantForm dimasukan ke 'form'
        if form.is_valid():
            form.save() # hasil inputan disimpan
            person = form.cleaned_data  # diambil data hasil inputan dengan: .cleaned_data, kemudian assign ke 'person'
            participant = form.cleaned_data['name'] # dari datanya, diambil data yang attribute 'name'
            activity.participants.create(name=participant)  # kemudian di create (add) participant untuk aktvitas yang terkait
            activity.save() # di save trus kelar deh :)))
            return redirect('/activities')
    
    context = {'form_participant':form}
    return render(request, 'activities/add_participant.html', context)

def details(request):
    list_activities = Activity.objects.all()
    return render(request, 'activities/details.html', {'list_activities': list_activities})