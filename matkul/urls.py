from django.urls import path

from . import views

app_name = 'matkul'

urlpatterns = [
    path('', views.list_matkul, name='list_matkul'),
    path('create-course', views.create_matkul, name='create_matkul'),
    path('update-course/<slug:slug>', views.update_matkul, name='update_matkul'),
    path('delete-course/<slug:slug>', views.delete_matkul, name='delete_matkul'),
    path('course-details/<slug:slug>', views.matkul_desc, name='matkul_desc')

]