# Generated by Django 3.1.2 on 2020-10-16 01:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('matkul', '0002_auto_20201015_1219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matkul',
            name='singkatan',
            field=models.SlugField(unique=True),
        ),
    ]
