from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User 
from django import forms

class CreateUserForm(UserCreationForm):
    
    password1 = forms.CharField(label= 'Password', widget=forms.PasswordInput())
    password2 = forms.CharField(label= 'Password Confirmation', widget=forms.PasswordInput())
    
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']

        

class LoginUserForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ['username', 'password']