from django.contrib import auth
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import *

# Create your tests here.
class AccountTest(TestCase):
    def test_sign_up_url_is_exist(self):
        response = Client().get('/accounts/signup/')
        self.assertEquals(response.status_code, 200)

    def test_login_url_is_exist(self):
        response = Client().get('/accounts/login/')
        self.assertEquals(response.status_code, 200)

    def test_logout_url_is_redirecting(self):
        response = Client().get('/accounts/logout/')
        self.assertEquals(response.status_code, 302)

    def test_signup_using_signup_function(self):
        found = resolve('/accounts/signup/')
        self.assertEquals(found.func, signup_view)

    def test_login_using_login_function(self):
        found = resolve('/accounts/login/')
        self.assertEquals(found.func, login_view)

    def test_logout_using_logout_function(self):
        found = resolve('/accounts/logout/')
        self.assertEquals(found.func, logout_view)

    def test_signup_page_using_template(self):
        response = Client().get('/accounts/signup/')
        self.assertTemplateUsed(response, "accounts/signup.html")

    def test_login_page_using_template(self):
        response = Client().get('/accounts/login/')
        self.assertTemplateUsed(response, "accounts/login.html")

    def test_signup_web_components(self):
        response = Client().get('/accounts/signup/')
        html_response = response.content.decode('utf8')
        self.assertIn("Sign Up", html_response)
        self.assertIn("Username", html_response)
        self.assertIn("Password", html_response)
        self.assertIn("Password Confirmation", html_response)

    def test_login_web_components(self):
        response = Client().get('/accounts/login/')
        html_response = response.content.decode('utf8')
        self.assertIn("Login", html_response)
        self.assertIn("Username", html_response)
        self.assertIn("Password", html_response)
        self.assertIn("Don't have account yet?", html_response)

class LogInTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)

    def test_login_form(self):
        form = LoginUserForm(data=self.credentials)
        self.assertTrue(form.is_valid())

    def test_login(self):
        response = self.client.post('/accounts/login/', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)        

    def test_logged_in_success(self):
        response = self.client.post('/accounts/login/', self.credentials, follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn("Hello, testuser!", html_response)

    def test_logged_in_users_page(self):
        self.client.login(username='testuser', password='secret')
        response = self.client.get('/book-search/', follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn("Search books here!", html_response)

    def test_not_logged_in_users_page(self):
        response = self.client.get('/book-search/')
        html_response = response.content.decode('utf8')
        self.assertIn("login first", html_response)

    def test_signup_is_not_allowed_for_logged_in_user(self):
        self.client.login(username='testuser', password='secret')
        response = self.client.get('/accounts/signup/')
        html_response = response.content.decode('utf8')
        self.assertIn("You're already logged in", html_response)


class SingUpTest(TestCase):
    def test_signup_url_redirecting(self):
        response = self.client.post("/accounts/signup/", 
        {
            'username': 'testuser',
            'password1': 'kappa4267',
            'password2': 'kappa4267',
        })
        self.assertEquals(response.status_code, 302)

    def test_signup_success(self):
        response = self.client.post("/accounts/signup/", 
        {
            'username': 'testuser',
            'password1': 'kappa4267',
            'password2': 'kappa4267',
        })
        self.assertTrue(response.wsgi_request.user.is_authenticated)

    def test_signup_form(self):
        form = CreateUserForm(data={
            'username': 'testuser',
            'password1': 'kappa4267',
            'password2': 'kappa4267',
        })
        self.assertTrue(form.is_valid())

    def test_signup_form_weak_password(self):
        form = CreateUserForm(data={
            'username': 'testuser',
            'password1': 'secret',
            'password2': 'secret',
        })
        self.assertFalse(form.is_valid())

    def test_signup_is_not_allowed_for_logged_in_user(self):
        self.client.post("/accounts/signup/", 
        {
            'username': 'testuser',
            'password1': 'kappa4267',
            'password2': 'kappa4267',
        })

        response = self.client.get('/accounts/signup/')
        html_response = response.content.decode('utf8')
        self.assertIn("You're already logged in", html_response)


        


