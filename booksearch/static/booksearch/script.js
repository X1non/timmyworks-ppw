$(document).ready( function(){
    $('#search-keywords').keyup( function(){
        var keyword = $(this).val();
        $.ajax({
            url: "/book-data?q=" + keyword,
            success: function(result){
                var books = result.items;
                // console.log(books);
                $('.books-info').empty();
                for(i = 0; i < books.length; i++){
                    var books_title = books[i].volumeInfo.title;

                    if(books[i].volumeInfo.authors === undefined){
                        var books_author = "-";
                    } else {
                        var books_author = books[i].volumeInfo.authors;
                    }
                    
                    if(books[i].volumeInfo.description === undefined){
                        var books_desc = "-";
                    } else {
                        var books_desc = books[i].volumeInfo.description;
                    }

                    var books_thumb = books[i].volumeInfo.imageLinks.smallThumbnail;
                    $('.books-info').append(
                    `<tr>   
                        <th class='book-title' scope="row"> ${books_title} </th>
                        <td class='book-author'> ${books_author} </td>
                        <td class='book-desc'> ${books_desc} </td>
                        <td> <img src="${books_thumb}"> </td>
                    </tr>`
                    )
                }        
            }
        });
    });
});