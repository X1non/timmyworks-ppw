from django.urls import path

from . import views

app_name = 'booksearch'

urlpatterns = [
    path('', views.index, name='index'),
]