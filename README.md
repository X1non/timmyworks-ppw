# [Timmyworks](https://timmyworks.up.railway.app/)

### Whats this? 🤔
[My portofolio website](https://timmyworks.up.railway.app/) built with Python web framework: [Django](https://www.djangoproject.com/).

### Background 📚
This was a project for Web Design & Programming course back on my second-year college as a Computer Science student.

Back in the day, this website was deployed on [Heroku](https://www.heroku.com/) and hosted on my [old repository](https://gitlab.com/X1non/story-four/). Since Heroku remove their [free-tiers program](https://blog.heroku.com/next-chapter), I have migrated and redeployed it on this repo by using [Railway](https://railway.app/).